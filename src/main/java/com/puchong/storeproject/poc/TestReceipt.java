/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.puchong.storeproject.poc;

import model.Customer;
import model.Product;
import model.Receipt;
import model.User;

/**
 *
 * @author User
 */
public class TestReceipt {
    public static void main(String[] args) {
        User seller = new User(1, "Jame", "password123", "123456789");
        Customer customer = new Customer(1, "Ten", "083000000");
        Product p1 = new Product(1,"Chathai",30);
        Product p2 = new Product(2,"Chayen",35);
        Receipt receipt = new Receipt(seller,customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 3);
        System.out.println(receipt);
        receipt.deleteReceiptDetail(0);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1, 2);
        System.out.println(receipt);
        receipt.addReceiptDetail(p1, 2);
        System.out.println(receipt);
    }
}
