/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Customer;
import model.Product;
import model.Receipt;
import model.ReceiptDetail;
import model.User;

/**
 *
 * @author User
 */
public class ReceiptDao implements DaoInterface<Receipt> {

    @Override
    public int add(Receipt object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO receipt (customer_id, user_id, total) VALUES (?, ?, ?);";
            PreparedStatement stml = conn.prepareStatement(sql);
            stml.setInt(1, object.getCustomer().getId());
            stml.setInt(2, object.getSeller().getId());
            stml.setDouble(3, object.getTotal());
            int row = stml.executeUpdate();
            ResultSet result = stml.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
                object.setId(id);
            }
            for(ReceiptDetail r:object.getReceiptdetail()){
                String sqlDetail = "INSERT INTO receipt_detail (receipt_id, product_id, price, amount) VALUES (?, ?, ?, ?);";
                PreparedStatement stmlDetail = conn.prepareStatement(sqlDetail);
                stmlDetail.setInt(1, r.getReceipt().getId());
                stmlDetail.setInt(2, r.getProduct().getId());
                stmlDetail.setDouble(3, r.getPrice());
                stmlDetail.setInt(4, r.getAmount());
                int rowDetail = stmlDetail.executeUpdate();
                ResultSet resultDetail = stml.getGeneratedKeys();
                if (resultDetail.next()) {
                    id = resultDetail.getInt(1);
                    r.setId(id);
                } 
            }
        } catch (SQLException ex) {
            System.out.println("Error: to create receipt.");
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Receipt> getAll() {
        ArrayList list = new ArrayList<>();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
            try {
            db.getConnection();
            conn.setAutoCommit(false);

            String sql = "SELECT r.id as id,\n"
                    + "       r.created as created,\n"
                    + "       customer_id,\n"
                    + "       c.name as customer_name,\n"
                    + "       c.tel as customer_tel,\n"
                    + "       user_id,\n"
                    + "       u.name as user_name,\n"
                    + "       u.tel as user_tel,\n"
                    + "       total\n"
                    + "  FROM receipt r,customer c, user u\n"
                    + "  WHERE r.customer_id = c.id AND r.user_id = u.id;"
                    + "  ORDER BY created DESC;";
            Statement stmt = conn.createStatement();
            ResultSet result = stmt.executeQuery(sql);

            while (result.next()) {
                int id = result.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerTel = result.getString("customer_tel");
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userTel = result.getString("user_tel");
                double total = result.getDouble("total");
                Receipt receipt = new Receipt(id, created, new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerTel));
                list.add(receipt);
            }

            result.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt"+ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date passing all receipt."+ex.getMessage());
        }
        return list;
    }

    @Override
    public Receipt get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            db.getConnection();
            conn.setAutoCommit(false);
            String sql = "SELECT r.id as id,\n"
                    + "       r.created as created,\n"
                    + "       customer_id,\n"
                    + "       c.name as customer_name,\n"
                    + "       c.tel as customer_tel,\n"
                    + "       user_id,\n"
                    + "       u.name as user_name,\n"
                    + "       u.tel as user_tel,\n"
                    + "       total\n"
                    + "  FROM receipt r,customer c, user u\n"
                    + "  WHERE r.id = ? AND r.customer_id = c.id AND r.user_id = u.id"
                    + "  ORDER BY created DESC;";
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet result = stmt.executeQuery();

            while (result.next()) {
                int rid = result.getInt("id");
                Date created = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(result.getString("created"));
                int customerId = result.getInt("customer_id");
                String customerName = result.getString("customer_name");
                String customerTel = result.getString("customer_tel");
                int userId = result.getInt("user_id");
                String userName = result.getString("user_name");
                String userTel = result.getString("user_tel");
                double total = result.getDouble("total");
                Receipt receipt = new Receipt(rid, created, new User(userId, userName, userTel),
                        new Customer(customerId, customerName, customerTel)
                );
                
                getReceiptDetail(conn, id, receipt);
                
                return receipt;
            }

            result.close();
            stmt.close();
            db.close();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to select receipt id =" + id + ": "+ ex.getMessage());
        } catch (ParseException ex) {
            System.out.println("Error: Date passing");
        }
        return null;
    }

    private void getReceiptDetail(Connection conn, int id, Receipt receipt) throws SQLException {
        String sqlDetail =  "SELECT rd.id as id,\n" +
                "       receipt_id,\n" +
                "       rd.product_id as product_id,\n" +
                "       p.name as product_name,\n" +
                "       p.price as product_price,\n" +
                "       rd.price as price,\n" +
                "       amount\n" +
                "  FROM receipt_detail rd, product p \n" +
                "  WHERE receipt_id = ? AND rd.product_id = p.id;"
                + "  ORDER BY created DESC;";
        PreparedStatement stmtDetail = conn.prepareStatement(sqlDetail);
        stmtDetail.setInt(1, id);
        ResultSet resultDetail = stmtDetail.executeQuery();
        while(resultDetail.next()){
            int receiptId = resultDetail.getInt("id");
            int productID = resultDetail.getInt("product_id");
            String productName = resultDetail.getString("product_name");
            double productPrice = resultDetail.getInt("product_price");
            double price = resultDetail.getInt("price");
            int amount = resultDetail.getInt("amount");
            Product product = new Product(productID, productName, productPrice);
            receipt.addReceiptDetail(receiptId, product, amount, price);
        }
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM receipt WHERE id = ?;";
            PreparedStatement stml = conn.prepareStatement(sql);
            stml.setInt(1, id);
            row = stml.executeUpdate();
        } catch (SQLException ex) {
            System.out.println("Error: Unable to delete receipt id "+id);        
        }
        db.close();
        return row;
    }

    @Override
    public int update(Receipt object) {
//        Connection conn = null;
//        Database db = Database.getInstance();
//        conn = db.getConnection();
//        int row = 0;
//        try {
//            String sql = "UPDATE Receipt SET name = ?, price = ? WHERE id = ?;";
//            PreparedStatement stml = conn.prepareStatement(sql);
//            stml.setString(1, object.getName());
//            stml.setDouble(2, object.getPrice());
//            stml.setInt(3, object.getId());
//            row = stml.executeUpdate();
//        } catch (SQLException ex) {
//            Logger.getLogger(TestSelectReceipt.class.getName()).log(Level.SEVERE, null, ex);
//        }
//        db.close();
        return 0;
    }

    public static void main(String[] args) {
        Product p1 = new Product(1,"Americano",30);
        Product p2 = new Product(2,"Coco",30);
        User seller = new User(1, "Puchong Sumalanukun", "password", "08300000");
        Customer customer = new Customer(1, "Jonny Meyer", "0823213121");
        Receipt receipt = new Receipt(seller,customer);
        receipt.addReceiptDetail(p1, 1);
        receipt.addReceiptDetail(p2, 3);
        
        ReceiptDao dao = new ReceiptDao();
        System.out.println("id =" + dao.add(receipt));
        System.out.println("Receipt after add: " + receipt);
        System.out.println("Get all: " + dao.getAll());
        
        Receipt newReceipt = dao.get(receipt.getId());
        System.out.println("New Receipt: "+newReceipt);
        dao.delete(receipt.getId());
        
    }
}
