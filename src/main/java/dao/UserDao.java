/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.puchong.storeproject.poc.TestSelectProduct;
import java.util.ArrayList;
import dao.UserDao;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.User;

/**
 *
 * @author User
 */
public class UserDao implements DaoInterface<User> {

    @Override
    public int add(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO user (name,password,tel) VALUES (?,?,?);";
            PreparedStatement stml = conn.prepareStatement(sql);
            stml.setString(1, object.getName());
            stml.setString(2, object.getPassword());
            stml.setString(3, object.getTel());
            int row = stml.executeUpdate();
            ResultSet result = stml.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<User> getAll() {
        ArrayList list = new ArrayList<>();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, password, tel FROM user";
            Statement stml = conn.createStatement();
            ResultSet result = stml.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                String password = result.getString("password");
                String tel = result.getString("tel");
                User user = new User(id, name, password, tel);
                list.add(user);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public User get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, password, tel FROM user WHERE id = " + id;
            Statement stml = conn.createStatement();
            ResultSet result = stml.executeQuery(sql);
            if (result.next()) {
                int uid = result.getInt("id");
                String name = result.getString("name");
                String password = result.getString("password");
                String tel = result.getString("tel");
                User user = new User(uid, name, password, tel);
                return user;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM user WHERE id = ?;";
            PreparedStatement stml = conn.prepareStatement(sql);
            stml.setInt(1, id);
            row = stml.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(User object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE user SET name = ?, password = ?, tel = ? WHERE id = ?;";
            PreparedStatement stml = conn.prepareStatement(sql);
            stml.setString(1, object.getName());
            stml.setString(2, object.getPassword());
            stml.setString(3, object.getTel());
            stml.setInt(4, object.getId());
            row = stml.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    public static void main(String[] args) {
        UserDao dao = new UserDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new User(-1, "Ben Tenis", "max1823", "0832100000"));
        System.out.println("id: " + id);
        User lastUser = dao.get(id);
        System.out.println("last user:" + lastUser);
        lastUser.setPassword("kewin11");
        lastUser.setTel("0830256481");
        int row = dao.update(lastUser);
        User updateUser = dao.get(id);
        System.out.println("update user:" + updateUser);
        dao.delete(id);
        User deleteUser = dao.get(id);
        System.out.println("delete user: " + deleteUser);
    }

}
