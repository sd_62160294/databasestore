/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dao;

import com.puchong.storeproject.poc.TestSelectProduct;
import database.Database;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.Product;

/**
 *
 * @author User
 */
public class ProductDao implements DaoInterface<Product> {

    @Override
    public int add(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int id = -1;
        try {
            String sql = "INSERT INTO product (name,price) VALUES (?,?);";
            PreparedStatement stml = conn.prepareStatement(sql);
            stml.setString(1, object.getName());
            stml.setDouble(2, object.getPrice());
            int row = stml.executeUpdate();
            ResultSet result = stml.getGeneratedKeys();
            if (result.next()) {
                id = result.getInt(1);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return id;
    }

    @Override
    public ArrayList<Product> getAll() {
        ArrayList list = new ArrayList<>();
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, price FROM product";
            Statement stml = conn.createStatement();
            ResultSet result = stml.executeQuery(sql);
            while (result.next()) {
                int id = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                Product product = new Product(id, name, price);
                list.add(product);
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return list;
    }

    @Override
    public Product get(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        try {
            String sql = "SELECT id, name, price FROM product WHERE id = " + id;
            Statement stml = conn.createStatement();
            ResultSet result = stml.executeQuery(sql);
            if (result.next()) {
                int pid = result.getInt("id");
                String name = result.getString("name");
                double price = result.getDouble("price");
                Product product = new Product(pid, name, price);
                return product;
            }
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }

    @Override
    public int delete(int id) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "DELETE FROM product WHERE id = ?;";
            PreparedStatement stml = conn.prepareStatement(sql);
            stml.setInt(1, id);
            row = stml.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    @Override
    public int update(Product object) {
        Connection conn = null;
        Database db = Database.getInstance();
        conn = db.getConnection();
        int row = 0;
        try {
            String sql = "UPDATE product SET name = ?, price = ? WHERE id = ?;";
            PreparedStatement stml = conn.prepareStatement(sql);
            stml.setString(1, object.getName());
            stml.setDouble(2, object.getPrice());
            stml.setInt(3, object.getId());
            row = stml.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(TestSelectProduct.class.getName()).log(Level.SEVERE, null, ex);
        }
        db.close();
        return row;
    }

    public static void main(String[] args) {
        ProductDao dao = new ProductDao();
        System.out.println(dao.getAll());
        System.out.println(dao.get(1));
        int id = dao.add(new Product(-1, "Thai tea", 40.0));
        System.out.println("id: " + id);
        Product lastProduct = dao.get(id);
        System.out.println("last product:" + lastProduct);
        lastProduct.setPrice(100);
        int row = dao.update(lastProduct);
        Product updateProduct = dao.get(id);
        System.out.println("update product:" + updateProduct);
        dao.delete(id);
        Product deleteProduct = dao.get(id);
        System.out.println("delete product: " + deleteProduct);
    }
}
